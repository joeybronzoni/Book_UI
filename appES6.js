/* Again -In order to get my form text to show I need to change the text input color to black. Its most likely only needed on my Arch Linux OS*/
const formTextTitle  = document.getElementById('title').style.color = 'black',
	  formTextAuthor = document.getElementById('author').style.color = 'black',
	  formTextISBN   = document.getElementById('isbn').style.color = 'black';


class Book {
  constructor(title, author, isbn) {
    this.title = title;
	this.author = author;
	this.isbn = isbn;
  }
}

/* The methods and the Event Listeners will all be the same from the constructor version of this */
class UI {
  addBookToList(book) {
	const list = document.getElementById('book-list');
	// Create <tr> element
	const row  = document.createElement('tr');
	// Insert cols
	row.innerHTML = `
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td>${book.isbn}</td>
    <td><a href "#" class="delete">X</a></td>
  `;

	//Now append the new book info to list
	list.appendChild(row);

  }

  showAlert(message, className) {
	// Create div for error
	const div = document.createElement('div');
	// Add classes
	div.className = `alert ${className}`;
	// Add text node
	div.appendChild(document.createTextNode(message));
	// Get parent element of form
	const container = document.querySelector('.container');
	// Get form element
	const form = document.querySelector('#book-form');
	// Insert alert
	container.insertBefore(div, form);

	// Timeeout after 3 seconds
	setTimeout(function() {
	  document.querySelector('.alert').remove();
	}, 3000);
  }

  deleteBook(target) {
	if (target.className === 'delete') {
	  target.parentElement.parentElement.remove();
	}
  }

  clearFields() {
	document.getElementById('title').value = '';
	document.getElementById('author').value = '';
	document.getElementById('isbn').value = '';
  }
};

// Local Storage Class
class Store {
  static getBooks() {
	let books;
	if (localStorage.getItem('books') === null) {
	  books = [];
	} else {
	  books = JSON.parse(localStorage.getItem('books'));
	}
	return books;
  }

  static displayBooks() {
	const books = Store.getBooks();
	books.forEach(function(book) {
	  const ui = new UI;

	  // Add book to UI
	  ui.addBookToList(book);
	});
  }

  static addBook(book) {
	/* We use the actual Class name Store(uppercase) because these are static methods, we don't need to instantiate it */
	const books = Store.getBooks();

	books.push(book);

	localStorage.setItem('books', JSON.stringify(books));
  }

  static removeBook(isbn) {
	/* Since we don't have id's we need something unique to call Stored the books buy */
	// console.log(isbn);
	const books = Store.getBooks();
	// loop through the books
	books.forEach(function(book, index) {
	  if (book.isbn === isbn) {
		books.splice(index, 1);
	}
	});

	// Set LocalStorage again
	localStorage.setItem('books', JSON.stringify(books));
  }
}

// DOM Load Event
document.addEventListener('DOMContentLoaded', Store.displayBooks());

// Event LIstener for a adding book
document.getElementById('book-form').addEventListener('submit', function(e) {
  // Get form values
  const title  = document.getElementById('title').value,
		author = document.getElementById('author').value,
		isbn   = document.getElementById('isbn').value;

  // Instantiate book constructor/object with the input values
  const book = new Book(title, author, isbn);

  // Instatiate UI
  const ui = new UI();
  console.log('ui in book-form Event Listener', ui);
  // Validate
  if (title === '' || author === '' || isbn === '') {
	// Error alert
	ui.showAlert('Please fill in all the fields', 'error');
  } else {
	// Add book to list
	ui.addBookToList(book);

	// Add To Local Storage
	Store.addBook(book);
	// Show success
	ui.showAlert('Book Added to Your List', 'success');

	// Clear fields
	ui.clearFields();

  }

  e.preventDefault();
});



// Event Listener for delete
document.getElementById('book-list').addEventListener('click', function(e) {

  // Instantiate UI object
  const ui = new UI();
  console.log('ui in book-list  Event Listener', ui);
  ui.deleteBook(e.target);

  // Remove from LocalStorage
  Store.removeBook(e.target.parentElement.previousElementSibling.textContent);
  // Show message for delete
  ui.showAlert('Book Removed from Table', 'success');

  e.preventDefault();
});
