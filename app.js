/* In order to get my form text to show I need to change the text input color to black. Its most likely only needed on my Arch Linux OS*/
const formTextTitle  = document.getElementById('title').style.color = 'black',
	  formTextAuthor = document.getElementById('author').style.color = 'black',
	  formTextISBN   = document.getElementById('isbn').style.color = 'black';

// TODO:
/* Add persistence with LocalStorage like the ES6-version
   -add Store Constructor
   -add 4 methods to the store Constructor
   -getbooks() displayBooks() addBooks() removeBooks()
*/

// Book Constructor
function Book(title, author, isbn) {
  this.title = title;
  this.author = author;
  this.isbn = isbn;
}

// UI Constructor
function UI() {};

// Add Book to List
UI.prototype.addBookToList = function(book) {
  const list = document.getElementById('book-list');
  // Create <tr> element
  const row  = document.createElement('tr');
  // Insert cols
  row.innerHTML = `
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td>${book.isbn}</td>
    <td><a href "#" class="delete">X</a></td>
  `;

  //Now append the new book info to list
  list.appendChild(row);

};
// Show Alert
UI.prototype.showAlert = function(message, className) {
  // Create div for error
  const div = document.createElement('div');
  // Add classes
  div.className = `alert ${className}`;
  // Add text node
  div.appendChild(document.createTextNode(message));
  // Get parent element of form
  const container = document.querySelector('.container');
  // Get form element
  const form = document.querySelector('#book-form');
  // Insert alert
  container.insertBefore(div, form);

  // Timeeout after 3 seconds
  setTimeout(function() {
	document.querySelector('.alert').remove();
  }, 3000);
};

// Delete Book
UI.prototype.deleteBook = function(target) {
  if (target.className === 'delete') {
	target.parentElement.parentElement.remove();
  }
};

// Clear fields after book is added
UI.prototype.clearFields = function() {
  document.getElementById('title').value = '';
  document.getElementById('author').value = '';
  document.getElementById('isbn').value = '';
};

// Event LIstener for a adding book
document.getElementById('book-form').addEventListener('submit', function(e) {
  // Get form values
  const title  = document.getElementById('title').value,
		author = document.getElementById('author').value,
		isbn   = document.getElementById('isbn').value;

  // Instantiate book constructor/object with the input values
  const book = new Book(title, author, isbn);

  // Instatiate UI
  const ui = new UI();
  console.log('ui in book-form Event Listener', ui);
  // Validate
  if (title === '' || author === '' || isbn === '') {
	// Error alert
	ui.showAlert('Please fill in all the fields', 'error');
  } else {
	// Add book to list
	ui.addBookToList(book);

	// Show success
	ui.showAlert('Book Added to Your List', 'success');

	// Clear fields
	ui.clearFields();

  }

  e.preventDefault();
});

//

/* Event Delegation is used when something shows up more than once with the same class or if something(an element) that is not there when the page loads but is created dynamiclly*/

// Event Listener for delete
document.getElementById('book-list').addEventListener('click', function(e) {

  // Instantiate UI object
  const ui = new UI();
  console.log('ui in book-list  Event Listener', ui);
  ui.deleteBook(e.target);

  // Show message for delete
  ui.showAlert('Book Removed from Table', 'success');

  e.preventDefault();
});
